#!/usr/bin/env python
import re
import os
import errno
import json
import time
import sys
import pandas as pd
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from bs4 import BeautifulSoup
from pyvirtualdisplay import Display

# sys.stdout = open('logs_1920_02_selenium.txt', 'w')

productCount = 0 # Products count
filesCount = 0 # HTML file parsed count

# chrome_path = "/Users/morriswong/Downloads/chromedriver" #most updated version of chromedriver
#options = webdriver.ChromeOptions()
#options.add_argument("test-type") #Path to your chrome profile
# driver = webdriver.Chrome(executable_path=chromedriver, chrome_options=options)
# driver = webdriver.Chrome(executable_path='./mac_chromedriver')

profiletry = webdriver.FirefoxProfile()
profiletry.set_preference("general.useragent.override","Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Mobile Safari/537.36")
display = Display(visible=0, size=(400, 681))
display.start()
profiletry = webdriver.FirefoxProfile()
profiletry.update_preferences()
driver = webdriver.Firefox(firefox_profile=profiletry)

#with open('./all_categories.json', 'r') as f:
#     data = json.load(f)

# with open('./categories_all.json', 'r') as f:
#      data = json.load(f)

print ('Your input:', str(sys.argv))
fileName = str(sys.argv[1])
category_directory = str(sys.argv[2])
df1 = pd.read_csv(fileName)
print ('Reading the csv: ' + fileName)

total_time = 0
for i in range(0, len(df1['category_name'])):
# for urls, category_name in data.items():
    print ('scraping begins...')
    category_name = df1['category_name'][i]
    category_id = df1['category_id'][i]
    print (category_id)
    urls = 'https://pricespy.co.uk/category.php?l=' + str(category_id)
    print ('For ' + category_name)
    category_name.replace('/', '-')
    all_href = [] # The "big array"
    previous_href = [] # The "small array"
    first_loop = True 
    pageCount = 0
    htmlCount = 0

    try: 
        driver.get(urls)
    except Exception: 
        continue

    try:
        category_matrix = driver.find_element_by_xpath("//*[@class='category-matrix']")
        print ('Page is a category page')
        continue
    except NoSuchElementException as err:
        print (err)
        print ('Can proceed crawling')

    try:
        last_page = driver.find_element_by_xpath("//*[@class='page-link last']").get_attribute("href")
        driver.get(last_page)
        last_url = driver.current_url
    except NoSuchElementException as err:
        last_url = driver.current_url

    regex = r'\d+$'
    matches = re.findall(regex, last_url)
    max_item = matches[0]
    loop_time = int((int(max_item) + 100) / 100)

    for i in range(0,loop_time):
        t0 = time.time()
        url = urls + '&s=' + str(pageCount) 
        try:
            driver.get(url)
        except (Exception, NoSuchElementException) as err:
            continue
        
        content = driver.page_source
        soup = BeautifulSoup(content,"html.parser")
        # Get links from the price comparason table
        table = soup.find_all('table')
        for tr in table:
            td = tr.find_all('td')
            for row in td:
                a_tags = row.find_all('a', href=True)
                if a_tags:
                    regex = r'\d+$'
                    matches = re.findall(regex, a_tags[0].get('href'))
                    # Handling multiple links in the same row: we only need one
                    # If the same, put together in small array to avoid duplicates
                    if matches and matches in previous_href: 
                        previous_href.append(matches)                                
                    # If not, add into big array and clean small array 
                    elif matches and not matches in previous_href:
                        # Add items into the empty array for the first time
                        if first_loop:
                            previous_href.append(matches)
                            first_loop = False
                        # Clear the small array
                        else:
                            all_href.append(previous_href[0][0])
                            previous_href = []
                            previous_href.append(matches)     
                        # Count of the number of products 
                        productCount += 1

        print ("The all_href array size: " + str(len(all_href)))
        if not os.path.exists('./' + category_directory):
            os.mkdir(category_directory)
        try:
            with open('./' + category_directory + '/all_href_' + category_name + '.json', 'w') as f:
                json.dump(all_href, f, indent=4, ensure_ascii=False)
        except Exception: 
            continue

        print ("The page count is: " + str(pageCount))
        print ("The html count is: " + str(htmlCount))
        # print ("The i count is: " + str(i))
        pageCount += 100
        htmlCount += 1
        i += 1
        t1 = time.time()

        time_per_loop = t1-t0
        total_time += time_per_loop
        print ("Time it takes (sec): " + str(time_per_loop))
        print ("Total time (sec): " + str(total_time))
        print ("Number of products: " + str(productCount))

driver.close()
