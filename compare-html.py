import filecmp

for i in range(1, 119):
    compare = filecmp.cmp(
        './Perfume/perfume' + str(i) + ".html",
        './Perfume/perfume' + str(i+1) + ".html",
        shallow=True
    )
    print ("i is: " + str(i))
    print (compare)