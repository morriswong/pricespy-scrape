import re
import os
import json
import requests
import datetime
import pandas as pd
import pprint 
import random
from random import shuffle
from bs4 import BeautifulSoup

def pricespy_redirect(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.content,'lxml')
    return(soup.find('a')['href'])

def save_page_source(kind, category):
    if not os.path.exists('./products_pages_source'):
        os.mkdir('products_pages_source')
    if not os.path.exists('./products_pages_source/' + category):
        os.mkdir('products_pages_source/' + category)
    with open("./products_pages_source/" + category_name + "/" + data[i] + '-' + kind + '-' + time + ".html", "w") as file:
        file.write(str(soup.encode('utf-8')))

def read_json(category_name):
    with open('./href/all_href_' + category_name +  '_priority.json', 'r') as f:
        data = json.load(f)
    return data

def read_all_categories_json():
    with open('./all_categories.json', 'r') as f:
        data_category = json.load(f)
    return data_category

df1 = pd.read_csv('high_priority_categories.csv')[0:]

# Count total number of products
total_no_of_products = 0
for i in range(0, len(df1))[:]:
    category_name = df1['sub_category'].iloc[i] 
    category_id = df1['category_id'].iloc[i]
    try:
        print(category_name)
        data = read_json(category_name)
        total_no_of_products += len(data)
    except OSError:
        continue
print ("There are in total " + str(total_no_of_products) + " products in this scrape")

total_time = 0
for i in range(0, len(df1))[:]:
    count = 0
    category_name = df1['sub_category'].iloc[i] 
    category_id = df1['category_id'].iloc[i]
    print ('scraping begins...')
    print ('Now scraping ' + category_name)
    category_name.replace('/', '-')
    #category_name.replace(' ', '_')

    data = []
    try:
        with open('./href/all_href_' + category_name +  '_priority.json', 'r') as f:
            data = json.load(f)      
    except OSError:
        print ("Can't find the file")
        continue

    all_products = []
    print ('The first product Id is ' + data[0])
    print ('The second product Id is ' + data[1])

    data = random.sample(data, len(data))
    print ('The first product Id is ' + data[0])
    print ('The second product Id is ' + data[1])
    print ('')
    for product_id in data[:70]:
        product = {}

        root_path = 'https://pricespy.co.uk/product.php?p='
        url = root_path + str(product_id) + '&c=all'
        print (url + " Added product")
        try:
            results = requests.get(url)
            soup = BeautifulSoup(results.content, "html.parser")
            product['productUrl'] = url
            time = '{:%Y-%m-%d-%H%M%S}'.format(datetime.datetime.now())
            save_page_source('product', category_name)
        except Exception:
            continue

        product['productId'] = product_id
        product['productUrl'] = url

        # Get product image
        for a_tag in soup.find_all('a', {'class' : 'img140'}):
            for img_tag in a_tag.find_all('img'):
                product['productImage'] = img_tag['src']
        
        # Get Category and Product Name
        for header in soup.find_all('div', {'id': 'page_header'}):
            items = header.find_all('span', {'itemprop':'name'})
            product['categoryPath'] = []
            for item in items:
                if item == items[-1]:
                    product['productName'] = item.text
                    "\\".join(product['categoryPath'])
                    break
                product['categoryPath'].append(item.text)

        # Get Brand Name
        for brand_name_div in soup.find_all('div', {'class': 'product-brand-box'}):
            for brand_name in brand_name_div.find_all('a'):
                product['brandName'] = brand_name.text

        # Get into table with details
        for table in soup.find_all('table', {'id': 'prislista'}):
            product["merchant"] = []
            row_count = 0
            for items in table.find_all('tr', {'class':'v-centered'}):
                
                merchants = items.find_all('span', {'class':'store-name-span'})
                prices = items.find_all('a', {'class':'price'})
                store_links = items.find_all('a', {'class':'success'})
            
                # Get Store name
                this_merchant = [merchant.text for merchant in merchants]

                # Get External Links
                for link in store_links:
                    link_redirect = 'https://pricespy.co.uk' + link['href']
                    this_merchant.append(link_redirect)

                # Get Price
                for price in prices:
                    this_merchant.append(price.text)
            
                if merchants:
                    row_count += 1
                    product["merchant"].append(this_merchant)
                    
            product["merchant_count"] = row_count
        
        # Get spec-table
        root_path = 'https://pricespy.co.uk/product.php?e='
        url = root_path + str(product_id)
        print (url + " Added spec")
        try:
            results  = requests.get(url)
            soup = BeautifulSoup(results.content, "html.parser")
            product['productSpecUrl'] = url
            save_page_source('spec', category_name)
        except Exception:
            continue

        for item in soup.find_all('div',{'class':'listcontainer'}):
            for tr in item.find_all('tr'):
                content = tr.find('td').get_text(', ').replace('Suggest a change','').replace('\xa0','').strip().strip(", ")
                content = content.replace('Contribute', 'N/a')
                header = tr.text.replace('Suggest a change','').replace('\xa0','').split('\n')
                header = [i.strip() for i in header if ((i !='')&(i.strip()!=content) )][0]
                if content:
                    product[header] = content
                else:
                    continue

        # Get the relevant page
        root_path = 'https://pricespy.co.uk/product.php?rel='
        url = root_path + str(product_id)
        print (url + " Added relevant")
        try:
            results  = requests.get(url)
            soup = BeautifulSoup(results.content, "html.parser")
            product['realtedProductsUrl'] = url 
            save_page_source('related', category_name)
        except Exception:
            continue

        all_products.append(product)
        #print (type(product))
        #print (product)
        count += 1
        print (str(count) + " out of " + str(len(data)) + " in " + category_name )
        print ('  ')

    #category_name = category_name.replace(' ) 
    category_name = re.sub(r"\s+", '_', category_name)
    print ("Writing file " + './all_' + category_name  + "_products" + '.json')
    print ('  ')
    with open('./all_' + category_name  + "_products" + '.json', 'a+') as f:
        json.dump(all_products, f, indent=4)
