import re
import json
import requests
import time
from selenium import webdriver
from bs4 import BeautifulSoup

with open('./all_categories.json', 'r') as f:
     data = json.load(f)

# for key, value in data.items():
#     print (key)
#     print (value)

# chrome_path = "/Users/morriswong/Downloads/chromedriver" #most updated version of chromedriver
pageCount = 0
htmlCount = 0
Perfume = "https://pricespy.co.uk/category.php?l=965&s=" + str(pageCount)

# load the page
first_page = requests.get(Perfume).content
# find the last href
soup = BeautifulSoup(first_page, "html.parser")
matched_a_tags = soup.find_all('a', {"class" : ["page-link", "last"]})
print ('start to find href')
for matched_a_tag in matched_a_tags:
    all_class = matched_a_tag.get('class')
    last_href = matched_a_tag.get('href')
    if 'page-link' in all_class and 'last' in all_class:
        print (last_href)
        # Find the number of pages
        regex = r'\d+$'
        matches = re.findall(regex, last_href)
        max_item = matches[0]
        no_of_pages = int((int(max_item) + 100) / 100)
        print (no_of_pages)

print ('start to loop pages')
total_time = 0
for i in range(0, no_of_pages):
    #  Record the time it takes for each parse
    t0 = time.time()
    Perfume = "https://pricespy.co.uk/category.php?l=965&s=" + str(pageCount)
    content = requests.get(Perfume).content
    soup = BeautifulSoup(content, "html.parser")
    print ("The page count is: " + str(pageCount))
    print ("The html count is: " + str(htmlCount))
    print ("The i count is: " + str(i))
    pageCount += 100
    htmlCount += 1
    i += 1
    with open("./Perfume/perfume" + str(htmlCount) + ".html", "w") as file:
        file.write(str(soup))
    t1 = time.time()
    time_per_loop = t1-t0
    total_time += time_per_loop
    print ("Time it takes: " + str(time_per_loop))
    print ("Total time: " + str(total_time))


