import re
import json
import requests
import datetime
from bs4 import BeautifulSoup

with open('./all_categories.json', 'r') as f:
     data_category = json.load(f)

total_time = 0
count = 0
for urls, category_name in data_category.items():
    if count == 5:
        break
    print ('scraping begins...')
    print ('For ' + category_name)
    category_name.replace('/', '-')
    print (urls)

    data = []
    with open('./href/all_href_' + category_name + '.json', 'r') as f:
        data = json.load(f)

    print ('The first product Id is ' + data[0])
    all_products = []

    print ('scraping now...')

    print (len(data))
    for i in range(0, len(data))[:2]:
        product = {}

        root_path = 'https://pricespy.co.uk/product.php?p='
        url = root_path + data[i] + '&c=all'
        print (url)
        results  = requests.get(url)
        soup = BeautifulSoup(results.content, "html.parser")
        product['productUrl'] = url
        time = '{:%Y-%m-%d-%H%M%S}'.format(datetime.datetime.now())
        with open("./" + data[i] + '-product-' + time + ".html", "w") as file:
            file.write(str(soup))
        
        product['productId'] = data[i]
        product['productUrl'] = url

        # Get product image
        for a_tag in soup.find_all('a', {'class' : 'img140'}):
            for img_tag in a_tag.find_all('img'):
                # print (img_tag['src'])
                product['productImage'] = img_tag['src']
        
        # Get Category and Product Name
        for header in soup.find_all('div', {'id': 'page_header'}):
            items = header.find_all('span', {'itemprop':'name'})
            product['categoryPath'] = []
            for item in items:
                if item == items[-1]:
                    # print ("BARND IS " + item.text)
                    product['productName'] = item.text
                    "\\".join(product['categoryPath'])
                    break
                # print (item.text)
                product['categoryPath'].append(item.text)

        # Get Brand Name
        for brand_name_div in soup.find_all('div', {'class': 'product-brand-box'}):
            for brand_name in brand_name_div.find_all('a'):
                # print (brand_name.text)
                product['brandName'] = brand_name.text

        # Get into table with details
        for table in soup.find_all('table', {'id': 'prislista'}):
            for items in table.find_all('tr', {'class':'v-centered'}):

                merchants = items.find_all('span', {'class':'store-name-span'})
                prices = items.find_all('a', {'class':'price'})
                store_links = items.find_all('a', {'class':'success'})
                
                # Get Store name
                for merchant in merchants:
                    # print (merchant.text)
                    product['merchantName'] = merchant.text

                # Get External Links
                for link in store_links:
                    link_redirect = 'https://pricespy.co.uk' + link['href']
                    product['merchanUrl'] = link_redirect

                # Get Price
                for price in prices:
                    # print (price.text)
                    product['price'] = price.text
        
        # Get spec-table
        root_path = 'https://pricespy.co.uk/product.php?e='
        url = root_path + data[i]
        print (url)
        results  = requests.get(url)
        soup = BeautifulSoup(results.content, "html.parser")
        product['productSpecUrl'] = url
        with open("./" + data[i] + '-spec-' + time + ".html", "w") as file:
            file.write(str(soup))

        for item in soup.find_all('div',{'class':'listcontainer'}):
            for tr in item.find_all('tr'):
                content = tr.find('td').text.replace('Suggest a change','').replace("\xa0",'').strip()
                header = tr.text.replace('Suggest a change','').replace('\xa0','').split('\n')
                header = [i.strip() for i in header if ((i !='')&(i.strip()!=content) )][0]
                # print (header) 
                if content:
                    # print (content)
                    product[header] = content
                else:
                    continue
                    # print ('----------------THIS IS THE TITLE')

        root_path = 'https://pricespy.co.uk/product.php?rel='
        url = root_path + data[0]
        print (url)
        results  = requests.get(url)
        soup = BeautifulSoup(results.content, "html.parser")
        product['realtedProductsUrl'] = url 
        with open("./" + data[i] + '-related-' + time + ".html", "w") as file:
            file.write(str(soup))

        all_products.append(product)

    count += 1
    print (count)

    with open('./all_' + category_name + '.json', 'w') as f:
        json.dump(all_products, f, indent=4)