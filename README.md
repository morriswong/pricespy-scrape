# The pricespy scrapers

### 01_get_categories.py

To run the this scraper, go to pricespy website and click on a certain categories to find the category id.
(e.g. https://pricespy.co.uk/category.php?l=956 is Beauty & Health, and hence the id is 956)

Then you can run the following:
python 01_get_categories.py `category_id` `output_categories` 

(e.g. python 01_get_categories.py `956` `beauty_Health_categories.csv` )

* The output would be a `csv` file with all the categories and their respective category id under that macro category

### 02_selenium_get_source.py

To get all the product id of the respective category.

python 02_selenium_get_source.py `some_categories.csv` `someCategories_productIDs`

(e.g. python 02_selenium_get_source.py `beauty_Health_categories.csv` `beauty_Health_productIDs`)

* The output would be a directory with respective category's `.json` of the product id in it

### get_product.py

To get all the product data.

python get_product.py `some_categories.csv` `someCategories_productIDs` `output directory` `start number (default type 0)` 2> `error_whatevername.log` &

(e.g. python 02_selenium_get_source.py `beauty_Health_categories.csv` `beauty_Health_productIDs` `beauty_health_data` `0` 2> `error_whatevername.log` &)

Things would usually breaks in this layer, to remedy:

1. `cd` to the `output directory` and type `ls -ltr` to see what is the last file crawlwed
2. Refer to the `input csv` and check which row it is at now by `sed -n x,yp somefile.csv`
3. Add back the `csv row number - 1` into the `start number` argument
