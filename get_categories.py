#!/usr/bin/env python
import json
import sys
import requests
import pandas as pd
from bs4 import BeautifulSoup
import logging
import pprint
from pprint import pformat
logging.basicConfig(filename='log_getCategories.txt',level=logging.DEBUG)

def get_page_soup(url):
    soup = requests.get(url).content
    page = BeautifulSoup(soup, "html.parser")
    return page

def get_category_urls(soup_Category):
    categories = []
    sub_Category = [item.findAll('h3') for item in soup_Category]
    for idx, links in enumerate(sub_Category[0][:-1]):
        if idx % 2 == 0:
            category_pair = []
            category_name = [link.text.strip() for link in links.findAll('a')]
            category_url = [link['href'] for link in links.findAll('a')]
            category_pair.append(category_name[0])
            url_id = category_url[0].split('=')
            category_pair.append(url_id[1])
            categories.append(category_pair)
    return categories

def category_check(li):
    dic['First'] = {}
    for i in range(0, len(li)):
        page = get_page_soup('https://pricespy.co.uk/category.php?l=' + li[i][1])
        isCategory = page.findAll('div', {'class':'category-matrix'})
        if isCategory:
            li[i].append('isCategory')
            dic['First'][li[i][0]] = get_category_urls(isCategory)
            print ('IS category ' + li[i][0] + ' ' + str(li[i][1]))
        else:
            li[i].append('notCategory')
            print ('Not category ' + li[i][0] + ' ' + str(li[i][1]))
    return li, dic

def get_into_categories(input_dict):
    root_url = 'https://pricespy.co.uk/category.php?l='
    total_li = []
    for outkey, value in input_dict.items():
        logging.info("Outside Key is " + str(outkey))
        if value:
            for inkey, value in value.items():
                dic[inkey] = {}
                for i in range(0, len(value)):
                    page = get_page_soup(root_url + value[i][1])
                    isCategory = page.findAll('div', {'class':'category-matrix'})
                    logging.info("Inside Key is " + str(inkey))
                    logging.info("i is " + str(i))
                    if isCategory:
                        value[i].append('isCategory')
                        dic[inkey][value[i][0]] = get_category_urls(isCategory)
                        logging.info(value[i][0])
                        logging.info('===========Is category===========')
                    else:
                        value[i].append('notCategory')
                        total_li.append(value[i])
                        logging.info(value[i][0] + ' Not category, Added to the total_li')
                        logging.info(pformat(total_li))
    return total_li, dic

print ('Your input:', str(sys.argv))
category_id = str(sys.argv[1])
fileName = str(sys.argv[2])
page = get_page_soup('https://pricespy.co.uk/category.php?k=' + category_id)
isCategory = page.find_all('div', {'class':'category-matrix'})

li = []
dic = {}
if isCategory:
    li = get_category_urls(isCategory)
    li, dic = category_check(li)
    count = 0
    while count < 5: # Prevent from entering infinite loop
        li_inner = []
        dic_inner = {}
        li_inner, dic_inner = get_into_categories(dic)
        if not li_inner: break # Would break if the first page has all categories
        li += li_inner
        dic = dic_inner
        count += 1

    headers = ['category_name', 'category_id', 'category_chk']
    df = pd.DataFrame(li, columns=headers)
    df.to_csv(fileName + '.csv')
    print (fileName + '.csv' + ' is created in the current directory.' )