import re
import json
import requests
from bs4 import BeautifulSoup

category_url = 'https://pricespy.co.uk/category.php'
results = requests.get(category_url)
all_categories = {}
big_categories_url_list = []

soup = BeautifulSoup(results.content,'html.parser')

def getCategoryId(item):
    regex = r'\d+$'
    matches = re.findall(regex, item.get('href'))
    if matches:
        id_no = matches[0]
        return id_no

table = soup.find_all('table')
for tr in table:
    td = tr.find_all('td')
    for row in td:
        a_tags = row.find_all('a', href=True)
        for a_tag in a_tags:
            regex = r'\d+$'
            matches = re.findall(regex, a_tag.get('href'))
            category = a_tag.text
            category_id = matches[0]
            category_url = "https://pricespy.co.uk/category.php?l=" + category_id
            category_tuple = [category, category_id]
            all_categories.update({category_url : category})

with open('./all_categories.json', 'w') as f:
     json.dump(all_categories, f, indent=4)