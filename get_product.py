#!/usr/bin/env python
import re
import os
import errno
import json
import time
import sys
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import requests
import datetime
import pandas as pd
from bs4 import BeautifulSoup
from pprint import pformat
import random
from random import shuffle
from pyvirtualdisplay import Display
import logging

print ('Your input: ', str(sys.argv))
input_csv = str(sys.argv[1])
input_directory = str(sys.argv[2])
output_directory = str(sys.argv[3])
starting_id = str(sys.argv[4])
logging.basicConfig(filename='log_' + output_directory + '.txt',level=logging.DEBUG)

def get_page_soup(url):
    soup = requests.get(url).content
    page = BeautifulSoup(soup, "html.parser")
    logger.info("========We got page!========")
    return page

def get_expansion_json(soup_test):
    scripts = soup_test.find_all('script')
    arr = str(scripts[8]).split('] =')
    new_arr = arr[2].split(';\nexpansion_data')
    return new_arr[0]

def get_extra_title(expand_json):
    li = []
    for keys, value in json.loads(expand_json).items():
        dic = {}
        dic['variations'] = []
        dic['merchant'] = value['foretag']['ftgnamn']
        key = 'paketpriser'
        if key in value:
            var_dic = {}
            var_dic['title'] = value['beskrivning']
            var_dic['redirect_id'] = value['pris_id']
            dic['variations'].append(var_dic)
            for items in value['paketpriser']:
                var_dic = {}
                var_dic['title'] = items['beskrivning']
                var_dic['redirect_id'] = items['pris_id']
                dic['variations'].append(var_dic)
        else:
            var_dic = {}
            var_dic['title'] = value['beskrivning']
            var_dic['redirect_id'] = value['pris_id']
            dic['variations'].append(var_dic)
        #print (dic)
        li.append(dic)
    return li

def get_category_urls(soup_Category):
    categories = []
    sub_Category = [item.findAll('h3') for item in soup_Category]
    for idx, links in enumerate(sub_Category[0][:-1]):
        if idx % 2 == 0:
            category_pair = []
            category_name = [link.text.strip() for link in links.findAll('a')]
            category_url = [link['href'] for link in links.findAll('a')]
            category_pair.append(category_name[0])
            regex = r'\d+$'
            matches = re.findall(regex, category_url[0])
            category_pair.append(matches[0])
            categories.append(category_pair)
    return categories

df1 = pd.read_csv(input_csv)

for i in range(0, len(df1['category_name']))[int(starting_id):]:
    if df1['category_chk'][i] == 'notCategory':
        logging.info (df1['category_name'][i])
        logging.info (df1['category_id'][i])
        count = 0
        crawl_count = 0
        category_name = df1['category_name'][i]
        category_id = df1['category_id'][i]
        logging.info ('scraping begins...')
        logging.info ('Now scraping ' + category_name)
        category_name.replace('/', '-')

        data = []
        #try:
        with open('./' + input_directory + '/all_href_' + category_name +  '.json', 'r') as f:
            data = json.load(f)      
        #except OSError:
            #logging.info ("Can't find the file")
            #logging.info (" ")
            #continue

        if not data: continue
        all_products = []
        data = random.sample(data, len(data))
        for product_id in data[:100]:
            logging.info (category_name)
            crawl_count += 1
            logging.info ('No. of products: ' + str(crawl_count) + ' in ' + str(len(data)))
            if len(data) <= 2: continue
            product = {}

            root_path = 'https://pricespy.co.uk/product.php?p='
            logging.info (product_id)
            url = root_path + str(product_id) + '&c=all'
            logging.info (url + " Added product")
            try:
                results = requests.get(url)
                soup = BeautifulSoup(results.content, "html.parser")
                #soup = get_page_soup(url)
                product['productUrl'] = url
                time = '{:%Y-%m-%d-%H%M%S}'.format(datetime.datetime.now())
                # save_page_source('product', category_name)
            except Exception:
                logging.info ('Product requests failed')
                continue

            product['productId'] = product_id
            product['productUrl'] = url

            # Get product image
            for a_tag in soup.find_all('a', {'class' : 'img140'}):
                for img_tag in a_tag.find_all('img'):
                    product['productImage'] = img_tag['src']
            
            # Get Category and Product Name
            for header in soup.find_all('div', {'id': 'page_header'}):
                items = header.find_all('span', {'itemprop':'name'})
                product['categoryPath'] = []
                for item in items:
                    if item == items[-1]:
                        product['productName'] = item.text
                        "\\".join(product['categoryPath'])
                        break
                    product['categoryPath'].append(item.text)

            # Get Brand Name
            for brand_name_div in soup.find_all('div', {'class': 'product-brand-box'}):
                for brand_name in brand_name_div.find_all('a'):
                    product['brandName'] = brand_name.text

            # Get into table with details
            for table in soup.find_all('table', {'id': 'prislista'}):
                #product["merchant"] = []
                row_count = 0
                for items in table.find_all('tr', {'class':'v-centered'}):
                    
                    merchants = items.find_all('span', {'class':'store-name-span'})
                    prices = items.find_all('a', {'class':'price'})
                    store_links = items.find_all('a', {'class':'success'})
                
                    # Get Store name
                    this_merchant = [merchant.text for merchant in merchants]

                    # Get External Links
                    for link in store_links:
                        link_redirect = 'https://pricespy.co.uk' + link['href']
                        this_merchant.append(link_redirect)

                    # Get Price
                    for price in prices:
                        this_merchant.append(price.text)
                
                    if merchants:
                        row_count += 1
                        #product["merchant"].append(this_merchant)
                
                expand_json = get_expansion_json(soup)
                if not json.loads(expand_json): continue
                arr = get_extra_title(expand_json)
                product['merchant'] = arr
                product["merchant_count"] = row_count
                 
            try:
                if product["merchant_count"] <= 1 or not product["merchant_count"]: continue
            except Exception:
                logging.info ('Merchant count undefined')
                continue

            # Get spec-table
            root_path = 'https://pricespy.co.uk/product.php?e='
            url = root_path + str(product_id)
            logging.info (url + " Added spec")
            try:
                results  = requests.get(url)
                soup = BeautifulSoup(results.content, "html.parser")
                #soup = get_page_soup(url)
                product['productUrl'] = url
                product['productSpecUrl'] = url
                # save_page_source('spec', category_name)
            except Exception:
                logging.info ('Spec requests failed')
                continue

            for item in soup.find_all('div',{'class':'listcontainer'}):
                for tr in item.find_all('tr'):
                    content = tr.find('td').get_text(', ').replace('Suggest a change','').replace('\xa0','').strip().strip(", ")
                    content = content.replace('Contribute', 'N/a')
                    header = tr.text.replace('Suggest a change','').replace('\xa0','').split('\n')
                    header = [i.strip() for i in header if ((i !='')&(i.strip()!=content) )][0]
                    if content:
                        product[header] = content
                    else:
                        continue

            # Get the relevant page
            root_path = 'https://pricespy.co.uk/product.php?rel='
            url = root_path + str(product_id)
            logging.info (url + " Added relevant")
            try:
                results  = requests.get(url)
                soup = BeautifulSoup(results.content, "html.parser")
                #########soup = get_page_soup(url)
                product['realtedProductsUrl'] = url 
                # save_page_source('related', category_name)
            except Exception:
                logging.info ('Related requests failed')
                continue

            all_products.append(product)
            logging.info (str(product).encode('utf-8'))
            count += 1
            logging.info (str(count) + " out of " + str(len(data)) + " is added in " + category_name )
            logging.info ('  ')

        category_name = category_name.replace(' ', '_')
        logging.info ("Writing file " + './all_' + category_name  + "_products" + '.json')
        logging.info ('  ')
        if not os.path.exists(output_directory): os.mkdir(output_directory) 
        with open('./' + output_directory  + '/all_' + category_name  + "_products" + '.json', 'a+') as f:
            logging.info('CREATING THE DIRECTORY ' + output_directory)
            json.dump(all_products, f, indent=4)
